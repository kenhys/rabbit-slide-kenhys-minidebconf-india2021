fabre.debian.net - An experiment about front-end of bugs.debian.org

### fabre.debian.net - An experiment about front-end of bugs.debian.org

Debian has a bug tracking system and bugs.debian.org is a key
infrastructure for Debian project.  It tracks many reports and the bug
number is assigned to each reports.  You can view such a report via
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=NNNNNN.

This bug tracking mechanism is working well for a long time. It is
very cool but it may be not friendly for newcomers.

#### Target audience

* Debian contributor or maintainer who want to fix bugs not only the
bugs which is related to maintained packages by you

It may not interesting for Debian developer because DD is already
familiar with bugs.debian.org, udd.debian.org, or qa.debian.org.

#### What will talk about

In this session, I’ll talk about one experiment about Debian's bug tracking mechanism.
It is running as https://fabre.debian.net nowadays.

Then, I'll explain recent updates since this experiment service was
mentioned at DebConf 2020 - "An experiment about personalized
front-end of bugs.debian.org" [1]

fabre.debian.net is constructed on traditional E-mail archives and
simple front-end for it.

#### References

* [1] "An experiment about personalized front-end of bugs.debian.org" DebConf 2020
  https://slide.rabbit-shocker.org/authors/kenhys/debconf2020-online/

* [2] "fabre.debian.net - An experiment about front-end of bugs.debian.org"
  https://slide.rabbit-shocker.org/authors/kenhys/minidebconf-india2021-online/
