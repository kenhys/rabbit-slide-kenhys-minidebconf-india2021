# fabre.debian.net \\n Finding untouched bugs

subtitle
:  E-mail archives + UDD metadata + Simple web front-end = 🤔

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  MiniDebConf India 2021 January 23, 2021

allotted-time
:   5m

theme
:  .

# NOTE: Presentation slide is published

* This presentation slide is available via Rabbit Slide Show
  * <https://slide.rabbit-shocker.org/authors/kenhys/minidebconf-india2021/>
    "fabre.debian.net - Finding untouched bugs"

# Personal profile

![](images/profile.png){:relative-height="40"}

* Newbie Debian Developer since September 2020 (@kenhys)
* Trackpoint(soft dome) and Wasa beef(Yamayoshi Wasabi Potato Chips) fan
* Working for ClearCode Inc.

# ClearCode Inc.

![](images/logo-combination-standard.svg){:relative-height="30"}

* <https://www.clear-code.com/>
  * Free software is important in ClearCode Inc.
  * We develop/support software with our free software development experiences
  * We feedback our business experiences to free software

# Lightning Talk Agenda

* Recall DebConf 2020 presentation
* The troublesome cases about bugs.d.o
* How to solve this situation?
* An experiment - fabre.debian.net

# Back to DebConf 2020

![](images/debconf2020-presentation.png){:relative-height="100"}


# The troublesome cases about bugs.d.o

* Mainly package or maintainer oriented approach:
  * <https://qa.debian.org/>
  * <https://tracker.debian.org/>
  * <https://udd.debian.org/dmd/>

* Not so easy cases: 🤔
  * find only untouched bugs
  * see blocked bugs at a glance 👀
  
# How to solve this situation?

* Mashup existing data + Simple front-end 💻
  * E-mail archives (debian-bugs-dist,debian-bugs-closed)  📧 
  * UDD metadata (udd-mirror@udd-mirror.debian.net) 🖥

# An experiment - fabre.debian.net

* Concept
  * *Make "unstable life" comfortable*
    * Fix a bug (by finding a bug that no one working on)
    * Triage a bug (by sending control E-mail easily)
    * Avoid pitfall bugs (by finding affected important bugs)

# fabre.debian.net

![](images/fabre-internals.png){:relative-height="100"}

# View recently updated bugs

![](images/updated-bugs.png){:relative-height="100"}


# Focus on untouched bugs

![](images/updated-untouched.png){:relative-height="100"}


# Focus on blocked bugs

![](images/blocked-bugs.png){:relative-height="100"}

# Updates since DebConf 2020

* Become a Debian Developer (September, 2020)
* Negotiated about <https://fabre.debian.net> subdomain with DSA (October, 2020)
* Got fabre.debian.net subdomain and deployed 🎉 (November, 2020)

# fabre.debian.net is sponsored by FOSSHOST


![](images/fosshost.org_Host_Light.png){:relative-height="80"}

* By migrating to <https://fosshost.org>, got more server resource! 🎉

# The next thing...

* Make things forward to beta:
  * TODO: Implement cool full-text search feature
  * TODO: Implement personalized search feature
